package io.gitlab.joasdev.openapi.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.CorsRegistry;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.config.WebFluxConfigurer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@EnableWebFlux
public class AppConfig implements WebFluxConfigurer {

    @Value("${app.cors.pathPattern:/**}")
    private String pathPattern;

    @Value("${app.cors.allowedOrigins:*}")
    private String[] allowedOrigins;

    @Value("${app.cors.allowedHeaders:*}")
    private String[] allowedHeaders;

    @Value("${app.cors.allowedMethods:*}")
    private String[] allowedMethods;

    @Value("${app.cors.maxAge:1800}")
    private long maxAge;

    @Override
    public void addCorsMappings(CorsRegistry corsRegistry) {

        log.info("pathPattern: {}", pathPattern);
        log.info("allowedOrigins: {}", Arrays.toString(allowedOrigins));
        log.info("allowedMethods: {}", Arrays.toString(allowedMethods));
        log.info("maxAge: {}", maxAge);

        corsRegistry.addMapping(pathPattern).allowedHeaders(allowedHeaders).allowedOrigins(allowedOrigins)
                .allowedMethods(allowedMethods).maxAge(maxAge);
    }

    /*
     * @Override public void addResourceHandlers(ResourceHandlerRegistry registry) {
     * registry.addResourceHandler("/swagger-ui.html**")
     * .addResourceLocations("classpath:/META-INF/resources/");
     * 
     * registry.addResourceHandler("/webjars/**")
     * .addResourceLocations("classpath:/META-INF/resources/webjars/"); }
     */

}
