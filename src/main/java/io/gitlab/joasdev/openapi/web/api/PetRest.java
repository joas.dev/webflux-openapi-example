package io.gitlab.joasdev.openapi.web.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.server.ServerWebExchange;

import io.gitlab.joasdev.openapi.generated.model.ApiResponseDto;
import io.gitlab.joasdev.openapi.generated.model.PetDto;
import io.gitlab.joasdev.openapi.generated.web.PetApi;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller
public class PetRest implements PetApi{

    @Override
    public Mono<ResponseEntity<PetDto>> addPet(@Valid Mono<PetDto> petDto, ServerWebExchange exchange) {
        // TODO Auto-generated method stub
        return PetApi.super.addPet(petDto, exchange);
    }

    @Override
    public Mono<ResponseEntity<Void>> deletePet(Long petId, String apiKey, ServerWebExchange exchange) {
        // TODO Auto-generated method stub
        return PetApi.super.deletePet(petId, apiKey, exchange);
    }

    @Override
    public Mono<ResponseEntity<Flux<PetDto>>> findPetsByStatus(@Valid String status, ServerWebExchange exchange) {
        // TODO Auto-generated method stub
        return PetApi.super.findPetsByStatus(status, exchange);
    }

    @Override
    public Mono<ResponseEntity<Flux<PetDto>>> findPetsByTags(@Valid List<String> tags, ServerWebExchange exchange) {
        // TODO Auto-generated method stub
        return PetApi.super.findPetsByTags(tags, exchange);
    }

    @Override
    public Mono<ResponseEntity<PetDto>> getPetById(Long petId, ServerWebExchange exchange) {
        // TODO Auto-generated method stub
        return PetApi.super.getPetById(petId, exchange);
    }

    @Override
    public Mono<ResponseEntity<PetDto>> updatePet(@Valid Mono<PetDto> petDto, ServerWebExchange exchange) {
        // TODO Auto-generated method stub
        return PetApi.super.updatePet(petDto, exchange);
    }

    @Override
    public Mono<ResponseEntity<Void>> updatePetWithForm(Long petId, @Valid String name, @Valid String status,
            ServerWebExchange exchange) {
        // TODO Auto-generated method stub
        return PetApi.super.updatePetWithForm(petId, name, status, exchange);
    }

    @Override
    public Mono<ResponseEntity<ApiResponseDto>> uploadFile(Long petId, @Valid String additionalMetadata,
            @Valid Mono<Resource> body, ServerWebExchange exchange) {
        // TODO Auto-generated method stub
        return PetApi.super.uploadFile(petId, additionalMetadata, body, exchange);
    }

}
