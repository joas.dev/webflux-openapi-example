package io.gitlab.joasdev.openapi.web.api;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.server.ServerWebExchange;

import io.gitlab.joasdev.openapi.generated.model.OrderDto;
import io.gitlab.joasdev.openapi.generated.web.StoreApi;
import reactor.core.publisher.Mono;

@Controller
public class StoreRest implements StoreApi {

    @Override
    public Mono<ResponseEntity<Void>> deleteOrder(Long orderId, ServerWebExchange exchange) {
        // TODO Auto-generated method stub
        return StoreApi.super.deleteOrder(orderId, exchange);
    }

    @Override
    public Mono<ResponseEntity<Map<String, Integer>>> getInventory(ServerWebExchange exchange) {
        // TODO Auto-generated method stub
        return StoreApi.super.getInventory(exchange);
    }

    @Override
    public Mono<ResponseEntity<OrderDto>> getOrderById(Long orderId, ServerWebExchange exchange) {
        // TODO Auto-generated method stub
        return StoreApi.super.getOrderById(orderId, exchange);
    }

    @Override
    public Mono<ResponseEntity<OrderDto>> placeOrder(@Valid Mono<OrderDto> orderDto, ServerWebExchange exchange) {
        // TODO Auto-generated method stub
        return StoreApi.super.placeOrder(orderDto, exchange);
    }

}
