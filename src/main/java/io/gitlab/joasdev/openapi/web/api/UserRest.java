package io.gitlab.joasdev.openapi.web.api;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.server.ServerWebExchange;

import io.gitlab.joasdev.openapi.generated.model.UserDto;
import io.gitlab.joasdev.openapi.generated.web.UserApi;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller
public class UserRest implements UserApi{

    @Override
    public Mono<ResponseEntity<UserDto>> createUser(@Valid Mono<UserDto> userDto, ServerWebExchange exchange) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Mono<ResponseEntity<UserDto>> createUsersWithListInput(@Valid Flux<UserDto> userDto,
            ServerWebExchange exchange) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Mono<ResponseEntity<Void>> deleteUser(String username, ServerWebExchange exchange) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Mono<ResponseEntity<UserDto>> getUserByName(String username, ServerWebExchange exchange) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Mono<ResponseEntity<String>> loginUser(@Valid String username, @Valid String password,
            ServerWebExchange exchange) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override    
    public Mono<ResponseEntity<Void>> logoutUser(ServerWebExchange exchange) {
        // TODO Auto-generated method stub
        return Mono.just(ResponseEntity.ok(null));
    }

    @Override
    public Mono<ResponseEntity<Void>> updateUser(String username, @Valid Mono<UserDto> userDto,
            ServerWebExchange exchange) {
        // TODO Auto-generated method stub
        return null;
    }

  

}
