# webflux-openapi-example

## Video Example
* [https://youtu.be/OlN03i8mD8o](https://youtu.be/OlN03i8mD8o)

## Social networks

* [Twitch](https://www.twitch.tv/tetradotoxina)
* [Youtube](https://www.youtube.com/channel/UCKGueVQMO7_YhifvFuRBb6g)
* [Fanpage Oficial (Entertainment)](https://bit.ly/3wHqavn)
* [Fanpage Developers (Developers)](https://bit.ly/3upIG9V)
* [Facebook Group (Developers)](https://bit.ly/3bTZaAQ)
* [Web](https://tetradotoxina.com)